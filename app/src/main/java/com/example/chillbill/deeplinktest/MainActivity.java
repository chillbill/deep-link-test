package com.example.chillbill.deeplinktest;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(Config.DEEP_LINKU));
        try {
            startActivity(i);
        } catch (Exception e) {
            Log.e("XXXXXXX:", e.getMessage());
        }

    }
}
